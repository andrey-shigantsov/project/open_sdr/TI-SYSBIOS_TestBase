/*
 * HostInterface.c
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#include <sys_log_cfg.h>
#define LOG_NAME "HOST IO"
#define LOG_LEVEL HOST_IO_LOG_LEVEL

#include "log.h"
#include "HostInterface.h"
#include "TestApp.h"

#include <SDR/host_interface/internal/io.h>

#include <xdc/runtime/Error.h>

#include <ti/sysbios/BIOS.h>

#define HANDLE_ERR_AND_RETURN(err,retval,msg...)\
  do{if(err) \
  { \
    LOG_FXN_TRACE(FAIL); \
    LOG(xTRACE,msg); \
    return retval; \
  }}while(0)

#define TX_LOCK_TIMEOUT 10000
static inline void tx_lock(HostInterface_t * This)
{
  if (!Semaphore_pend(This->txMutex,TX_LOCK_TIMEOUT))
  {
    LOG(WARNING,"TX: lock timeout exited, wait forever...");
    Semaphore_pend(This->txMutex, BIOS_WAIT_FOREVER);
    LOG(WARNING,"TX: locked");
  }
}
static inline void tx_unlock(HostInterface_t * This){Semaphore_post(This->txMutex);}


Size_t HOST_IO_raw_send(HOST_IO_t * IO, const char * Buf, const Size_t Count)
{
  LOG_FXN_TRACE(START);
  HostInterface_t * This = (HostInterface_t *)IO->cfg.Master;

  UInt8_t header[SDR_PROTO_HEADER_BUF_SIZE];
  proto_raw_write_header(Count, header,SDR_PROTO_HEADER_BUF_SIZE);

  bool err = false;
  tx_lock(This);
  int wn = drv_uart_write(&This->drvUart, (const char *)header, SDR_PROTO_HEADER_BUF_SIZE);
  err |= (wn < SDR_PROTO_HEADER_BUF_SIZE);
  wn = drv_uart_write(&This->drvUart, Buf,Count);
  err |= (wn < Count);
  UInt8_t finish = SDR_PROTO_FINISH_SYM;
  wn = drv_uart_write(&This->drvUart, (const char *)&finish,1);
  err |= (wn < 1);
  tx_unlock(This);
  HANDLE_ERR_AND_RETURN(err,0,"write failure");
  return Count;
}

Size_t HOST_IO_raw_send_buf(HOST_IO_t * IO, const UInt8_t * Buf, const Size_t Count)
{
  return HOST_IO_raw_send(IO, (const char *)Buf, Count);
}

Size_t HOST_IO_raw_send_text(HOST_IO_t * IO, const char * Buf, const Size_t Count)
{
  return HOST_IO_raw_send(IO, Buf, Count);
}

static void rx_buf_handler(HostInterface_t * This, char * buf, int count)
{
  LOG_FXN_TRACE(START);
  SDR_ASSERT(count > 0);
  for (int i = 0; i < count; ++i)
    proto_reader(&This->proto.reader, buf[i]);
  LOG_FXN_TRACE(FINISH);
}
static void reader_warning_handler(HostInterface_t * This, ProtoReaderWarning_t id)
{
  switch (id)
  {
  case proto_reader_wrn_Null: break;
  case proto_reader_wrn_Generic:
    LOG(WARNING, "rx buf handler: failure");
    break;
  case proto_reader_wrn_FinishSymInvalid:
    LOG(WARNING, "rx buf handler: finish symbol invalid");
    break;
  case proto_reader_wrn_SyncLost:
    LOG(WARNING, "rx buf handler: sync lost");
    break;
  case proto_reader_wrn_ResetForHeader:
    LOG(WARNING, "rx buf handler: reset: valid header found");
    break;
  case proto_reader_wrn_crcForHeader:
    LOG(WARNING, "rx buf handler: header crc failure");
    break;
  }
}

static void reader_buf_handler(HostInterface_t * This, UInt8_t * buf, Size_t size)
{
  HOST_IO_Result_t res = HOST_IO_raw_rx_buf_handler(&This->io, buf, size);
  switch (res)
  {
  default: break;
  case host_io_FAIL:
    LOG(WARNING, "rx buf handler: failure");
    break;

  case host_io_FAIL_CRC:
    LOG(WARNING, "rx buf handler: crc failure");
    break;
  }
}

static void params_handler(HostInterface_t * This, TARGET_Params_t * Params)
{
  LOG_FXN_TRACE(START);
  TestApp_refreshParams(Params);
  LOG_FXN_TRACE(FINISH);
}

static void started_handler(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  TestApp_prestart();
  LOG(INFO,"started");
  LOG_FXN_TRACE(FINISH);
}
static void paused_handler(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  LOG(INFO,"paused");
  LOG_FXN_TRACE(FINISH);
}
static void continued_handler(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  LOG(INFO,"continued");
  LOG_FXN_TRACE(FINISH);
}
static void stopped_handler(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  LOG(INFO,"stopped");
  LOG_FXN_TRACE(FINISH);
}
static void step_handler(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  TestApp_step();
  LOG(INFO,"stepped");
  LOG_FXN_TRACE(FINISH);
}
static void reseted_handler(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  TestApp_reset();
  LOG(INFO,"reseted");
  LOG_FXN_TRACE(FINISH);
}

static void send_all(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  SDR_UNUSED(This);
  TestApp_sendAll();
  LOG_FXN_TRACE(FINISH);
}

static void send_format(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);
  SDR_UNUSED(This);
  TestApp_sendFormats();
  LOG_FXN_TRACE(FINISH);
}

static void set_value(HostInterface_t * This, UInt8_t id, const char * val, Size_t size)
{
  LOG_FXN_TRACE(START);
  SDR_UNUSED(This);
  TestApp_setValue(id,val,size);
  LOG_FXN_TRACE(FINISH);
}

bool init_HostInterface(HostInterface_t * This)
{
  LOG_FXN_TRACE(START);

  Error_Block eb;
  Error_init(&eb);
  This->txMutex = Semaphore_create(1, NULL, &eb);
  if (Error_check(&eb))
    LOG(ERROR, "create semaphore failure");

  HOST_IO_Cfg_t cfgIO;
  cfgIO.Master = This;
  cfgIO.on_params_changed = (HOST_IO_params_changed_t)params_handler;
  cfgIO.on_started = (event_sink_t)started_handler;
  cfgIO.on_paused = (event_sink_t)paused_handler;
  cfgIO.on_continued = (event_sink_t)continued_handler;
  cfgIO.on_stopped = (event_sink_t)stopped_handler;
  cfgIO.on_step = (event_sink_t)step_handler;
  cfgIO.on_reset = (event_sink_t)reseted_handler;
  cfgIO.on_get_all = (event_sink_t)send_all;
  cfgIO.on_format_request = (event_sink_t)send_format;
  cfgIO.on_set_value = (HOST_IO_set_value_t)set_value;
  init_HOST_IO(&This->io,&cfgIO);

  Drv_UART_Cfg_t cfgDrvUart;
  cfgDrvUart.rxBuf = This->drvUartBuf;
  cfgDrvUart.rxBufSize = SYS_HOST_INFERFACE_UART_RX_BUFSIZE;
  Drv_UART_Cbk_t cbk;
  cbk.on_ready_read = (drv_uart_rx_buf_ready_fxn_t)rx_buf_handler;
  if (!init_Drv_UART(&This->drvUart, &cfgDrvUart)) return 0;
  Drv_UART_setCallback(&This->drvUart, This, &cbk);

  ProtoReaderCfg_t cfgReader;
  init_ProtoReaderCfg(&cfgReader);
  cfgReader.buf.P = (UInt8_t *)This->proto.readerBuf;
  cfgReader.buf.Size = SYS_HOST_INFERFACE_UART_PROTO_BUFSIZE;
  init_ProtoReader(&This->proto.reader, &cfgReader);
  ProtoReaderCallback_t cbkReader;
  init_ProtoReaderCallback(&cbkReader);
  cbkReader.on_content_ready = (bytes_sink_t)reader_buf_handler;
  cbkReader.on_warning = (proto_reader_warning_fxn_t)reader_warning_handler;
  ProtoReader_setCallback(&This->proto.reader, This, &cbkReader);

  LOG_FXN_TRACE(FINISH);
  return 1;
}
