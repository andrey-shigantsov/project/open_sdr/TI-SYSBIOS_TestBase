/*
 * HostInterface.h
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_HOSTINTERFACE_H_
#define SYS_HOSTINTERFACE_H_

#include "common.h"
#include "DRV/uart.h"
#include <SDR/host_interface/base/proto.h>
#include <SDR/host_interface/io.h>

#include <ti/sysbios/knl/Semaphore.h>

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  HOST_IO_t io;

  Drv_UART_t drvUart;
  char drvUartBuf[SYS_HOST_INFERFACE_UART_RX_BUFSIZE];

  struct
  {
    ProtoReader_t reader;
    char readerBuf[SYS_HOST_INFERFACE_UART_PROTO_BUFSIZE];
  } proto;

  Semaphore_Handle txMutex;
} HostInterface_t;

bool init_HostInterface(HostInterface_t * This);

#ifdef __cplusplus
}
#endif

#endif /* SYS_HOSTINTERFACE_H_ */
