/*
 * TestApp.h
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_TESTAPP_H_
#define SYS_TESTAPP_H_

#include "common.h"
#include <SDR/TestBase/TestData.h>

#include <SDR/host_interface/base/target.h>

#ifdef __cplusplus
extern "C"{
#endif

bool init_TestApp(TestData_t * TestData, int priority, int32_t exec_timeout_ms);

void TestApp_sendAll();

void TestApp_sendFormats();
void TestApp_refreshParams(TARGET_Params_t * Params);

void TestApp_setExecTimeoutMs(int32_t timeout_ms);
void TestApp_setValue(UInt8_t id, const char * val, Size_t size);

void TestApp_prestart();
void TestApp_step();
void TestApp_reset();

bool TestApp_TaskIsRunning();

#ifdef __cplusplus
}
#endif

#endif /* SYS_TESTAPP_H_ */
