/*
 * TestApp.c
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#include <sys_log_cfg.h>
#define LOG_NAME "TestApp"
#define LOG_LEVEL TESTAPP_LOG_LEVEL

#include "log.h"
#include "TestApp.h"

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/knl/Semaphore.h>

#include <sdr_host_io.h>
#include <SDR/TestBase/TestIO.h>

#include <stdio.h>

typedef struct
{
  bool flag_IsRunning, flag_doStep, flag_WakeupAfterTimeout, flag_WakeupAfterTimeout0;
  uint32_t SleepTicks;

  Task_Handle task;
  Mailbox_Handle mbox;
  Semaphore_Handle mutex, WakeUpSem;

  TestData_t * TestData;

  char testTmpFormat[TARGET_TMP_FORMAT_MAXLEN];
} TestApp_t;

typedef enum
{
  TestApp_cmdRefreshTargetParams,
  TestApp_cmdSetExecTimeout,
  TestApp_cmdSendPlotsFormat
} TestAppMsgType_t;
typedef struct
{
  TestAppMsgType_t type;
  union
  {
    TARGET_Params_t target_params;
    int32_t timeout_ms;
  } data;
} TestAppMsg_t;

static void TestApp_private_setExecTimeoutMs(int32_t timeout_ms);
static void TestApp_private_sendParams();
static void TestApp_private_sendName();
static void TestApp_private_sendControlFormat();
static void TestApp_private_sendStatusFormat();
static void TestApp_private_sendPlotsFormat();
static void TestApp_private_sendState();
static void TestApp_private_refreshTestState();

static TestApp_t TestApp;

static void test_task_fxn(UArg a0, UArg a1);
bool init_TestApp(TestData_t * TestData, int priority, int32_t exec_timeout_ms)
{
  LOG_FXN_TRACE(START);

  TestApp.flag_IsRunning = false;
  TestApp.flag_doStep = false;
  TestApp.flag_WakeupAfterTimeout = false;
  TestApp.flag_WakeupAfterTimeout0 = false;
  TestApp_private_setExecTimeoutMs(exec_timeout_ms);

  Error_Block eb;

#if 0
  Error_init(&eb);
  Mailbox_Params mboxParams;
  Mailbox_Params_init(&mboxParams);
  TestApp.mbox = Mailbox_create(sizeof(TestAppMsg_t), SYS_TASK_TEST_MBOX_NUM_BUFS, &mboxParams, &eb);
  if (Error_check(&eb))
  {
    LOG(ERROR, "create mailbox failure");
    return false;
  }
#endif

  Error_init(&eb);
  TestApp.mutex = Semaphore_create(1, NULL, &eb);
  if (Error_check(&eb))
  {
    LOG(ERROR, "create mutex semaphore failure");
    return false;
  }

  Error_init(&eb);
  TestApp.WakeUpSem = Semaphore_create(1, NULL, &eb);
  if (Error_check(&eb))
  {
    LOG(ERROR, "create wake up semaphore failure");
    return false;
  }

  Error_init(&eb);
  Task_Params tskParams;
  Task_Params_init(&tskParams);
  tskParams.instance->name = "TestApp";
  tskParams.priority = priority;
  tskParams.arg0 = (UArg) TestData;
  TestApp.task = Task_create(test_task_fxn, &tskParams, &eb);
  if (Error_check(&eb))
  {
    LOG(ERROR, "create task failure");
    return false;
  }

  LOG_FXN_TRACE(FINISH);
  return (TestApp.task != NULL);
}

//static inline void TestApp_sendMsg(TestAppMsg_t * msg){Mailbox_post(TestApp.mbox, msg, BIOS_WAIT_FOREVER);}

#define LOCK_TIMEOUT 10000
static inline xdc_Bool TestApp_lock()
{
  xdc_Bool res = Semaphore_pend(TestApp.mutex, LOCK_TIMEOUT);
  return res;
}
static inline void TestApp_unlock()
{
  Semaphore_post(TestApp.mutex);
}

#define LOG_FXN_LOCK_FAIL \
  do{ \
    LOG_FXN(ERROR,"lock failure"); \
  }while(0)

#define PUBLIC_LOCK_OR_RETURN \
    LOG_FXN(TRACE,"lock"); \
    if (!TestApp_lock()) \
    { \
      LOG_FXN_LOCK_FAIL; \
      LOG_FXN_TRACE(FAIL); \
      return; \
    }

void TestApp_sendAll()
{
  LOG_FXN_TRACE(START);
  PUBLIC_LOCK_OR_RETURN;
  TestApp_private_sendParams();
  TestApp_private_sendName();
  TestApp_private_sendStatusFormat();
  TestApp_private_sendPlotsFormat();
  TestApp_private_sendControlFormat();
  TestApp_private_sendState();
  TestApp_private_refreshTestState();
  TestApp_unlock();
  LOG_FXN_TRACE(FINISH);
}

void TestApp_sendFormats()
{
  LOG_FXN_TRACE(START);
  PUBLIC_LOCK_OR_RETURN;
  TestApp_private_sendName();
  TestApp_private_sendStatusFormat();
  TestApp_private_sendPlotsFormat();
  TestApp_private_sendControlFormat();
  TestApp_unlock();
  LOG_FXN_TRACE(FINISH);
}

void TestApp_refreshParams(TARGET_Params_t * Params)
{
  LOG_FXN_TRACE(START);
  PUBLIC_LOCK_OR_RETURN;
  TestApp_private_setExecTimeoutMs(Params->timeout_ms);
  TestApp_unlock();
  LOG_FXN_TRACE(FINISH);
}

void TestApp_setExecTimeoutMs(int32_t timeout_ms)
{
  LOG_FXN_TRACE(START);
  PUBLIC_LOCK_OR_RETURN;
  TestApp_private_setExecTimeoutMs(timeout_ms);
  TestApp_unlock();
  LOG_FXN_TRACE(FINISH);
}

void TestApp_setValue(UInt8_t id, const char * val, Size_t size)
{
  LOG_FXN_TRACE(START);
  LOG(INFO,"set value #%x(\"%s\") to \"%s\"",id,testdata_ValueIdString(TestApp.TestData,id),val);
  PUBLIC_LOCK_OR_RETURN;
  testdata_set_value(TestApp.TestData, id, val, size);
  TestApp_unlock();
  LOG_FXN_TRACE(FINISH);
}

void TestApp_prestart()
{
  LOG_FXN_TRACE(START);
  PUBLIC_LOCK_OR_RETURN;
  testdata_prestart(TestApp.TestData);
  TestApp_unlock();
  LOG_FXN_TRACE(FINISH);
}

void TestApp_step()
{
  LOG_FXN_TRACE(START);

  TestApp.flag_doStep = true;
  Semaphore_post(TestApp.WakeUpSem);

  LOG_FXN_TRACE(FINISH);
}

void TestApp_reset()
{
  LOG_FXN_TRACE(START);

  PUBLIC_LOCK_OR_RETURN;
  testdata_reset(TestApp.TestData);
  TestApp_unlock();

  LOG_FXN_TRACE(FINISH);
}

bool TestApp_TaskIsRunning()
{
  return TestApp.flag_IsRunning;
}

//----------------------------------------------------------------------------------

static void TestApp_private_setExecTimeoutMs(int32_t timeout_ms)
{
  TestApp.SleepTicks = system_ticks_count_ms(timeout_ms);
  LOG(INFO,"set timeout to %dms", timeout_ms);
}

static void TestApp_private_sendParams()
{
  TARGET_Params_t Params;
  Params.timeout_ms = TestApp.SleepTicks/system_ticks_count_ms(1);
  HOST_IO_send_params(HOST_IO(), &Params);
  LOG(INFO,"send params: \"timeout %dms\"", Params.timeout_ms);
}

static void TestApp_private_sendName()
{
  HOST_IO_send_name(HOST_IO(), testdata_name(TestApp.TestData));
  LOG(INFO,"send name: \"%s\"", testdata_name(TestApp.TestData));
}

static void TestApp_private_sendStatusFormat()
{
  testdata_status_format(TestApp.TestData, TestApp.testTmpFormat, TARGET_TMP_FORMAT_MAXLEN);
  HOST_IO_send_format(HOST_IO(), TARGET_statusFORMAT, TestApp.testTmpFormat);
  LOG(INFO,"send status format: \"%s\"", TestApp.testTmpFormat);
}

static void TestApp_private_sendControlFormat()
{
  testdata_control_format(TestApp.TestData, TestApp.testTmpFormat, TARGET_TMP_FORMAT_MAXLEN);
  HOST_IO_send_format(HOST_IO(), TARGET_controlFORMAT, TestApp.testTmpFormat);
  LOG(INFO,"send control format: \"%s\"", TestApp.testTmpFormat);
}

static void TestApp_private_sendPlotsFormat()
{
  testdata_plots_format(TestApp.TestData, TestApp.testTmpFormat, TARGET_TMP_FORMAT_MAXLEN);
  HOST_IO_send_format(HOST_IO(), TARGET_plotsFORMAT, TestApp.testTmpFormat);
  LOG(INFO,"send plots format: \"%s\"", TestApp.testTmpFormat);
}

static void TestApp_private_sendState()
{
  HOST_IO_send_state_response(HOST_IO(), HOST_IO_getState(HOST_IO()));
  LOG(INFO,"send state");
}

static void TestApp_private_refreshTestState()
{
  testdata_refresh_state(TestApp.TestData);
}

static void TestApp_private_step()
{
  if (!HOST_IO_isStarted(HOST_IO()))
    HOST_IO_startPaused(HOST_IO());

  PUBLIC_LOCK_OR_RETURN;
  testdata_exec(TestApp.TestData);
  TestApp_unlock();
}


static void TestApp_private_WaitAndOther(uint32_t ticks_count)
{
#if 0
  TestAppMsg_t msg;
  if (Mailbox_pend(TestApp.mbox, &msg, timeout))
  {
    switch(msg.type)
    {
    default:
      break;
    }
  }
#else
  xdc_Bool semWakeUp = false, WakeUp = false;
  do
  {
    if (semWakeUp && TestApp.flag_doStep)
    {
      TestApp_private_step();
      TestApp.flag_doStep = false;
    }
    else if (WakeUp)
    {
      PUBLIC_LOCK_OR_RETURN;
      testdata_sys_wakeup(TestApp.TestData);
      TestApp_unlock();
      if (TestApp.flag_WakeupAfterTimeout0)
      {
        TestApp.flag_WakeupAfterTimeout = false;
        TestApp.flag_WakeupAfterTimeout0 = false;
      }
    }
    semWakeUp = Semaphore_pend(TestApp.WakeUpSem, ticks_count);
    TestApp.flag_WakeupAfterTimeout0 = !semWakeUp && TestApp.flag_WakeupAfterTimeout;
    WakeUp = semWakeUp || TestApp.flag_WakeupAfterTimeout0;
  } while (WakeUp);
#endif
}

static void test_task_fxn(UArg a0, UArg a1)
{
  SDR_UNUSED(a1);

  LOG_FXN_TRACE(START);

  TestApp.TestData = (TestData_t *)a0;

  testdata_sys_init(TestApp.TestData);

  LOG_FXN_TRACE(RUNNING);
  HOST_IO_send_response(HOST_IO(), TARGET_systemStarted);

  TestApp.flag_IsRunning = 1;
  while(TestApp.flag_IsRunning)
  {
    if (!TestApp_lock())
    {
      LOG_FXN_LOCK_FAIL;
      continue;
    }

    if (HOST_IO_isRunning(HOST_IO()))
    {
      testdata_exec(TestApp.TestData);
    }

    uint32_t timeout = TestApp.SleepTicks;

    TestApp_unlock();

    TestApp_private_WaitAndOther(timeout);
  }

  LOG_FXN_TRACE(FINISH);
}

//----------------------------------------------------------------------------------

void test_setExecTimeoutMs(int32_t timeout)
{
  TestApp_private_setExecTimeoutMs(timeout);
  TestApp_private_sendParams();
}

void test_startPaused()
{
  testdata_prestart(TestApp.TestData);
  HOST_IO_setTargetState(HOST_IO(),TARGET_Started);
}

void test_continue()
{
  HOST_IO_setTargetState(HOST_IO(),TARGET_Running);
}

void test_start()
{
  if (HOST_IO_isStarted(HOST_IO()))
  {
    HOST_IO_setTargetState(HOST_IO(),TARGET_Running);
    return;
  }
  testdata_prestart(TestApp.TestData);
  HOST_IO_setTargetState(HOST_IO(),TARGET_Running);
}

void test_pause()
{
  HOST_IO_setTargetState(HOST_IO(),TARGET_Paused);
}

void test_reset()
{
  testdata_reset(TestApp.TestData);
}

void test_stop()
{
  test_reset();
  HOST_IO_setTargetState(HOST_IO(),TARGET_Stopped);
}

void test_sys_wakeup()
{
  Semaphore_post(TestApp.WakeUpSem);
}

void test_sys_wakeup_after_timeout()
{
  TestApp.flag_WakeupAfterTimeout = true;
}

Bool_t test_sys_WakeupIsAfterTimeout()
{
  return TestApp.flag_WakeupAfterTimeout0;
}

Bool_t test_isStarted()
{
  return HOST_IO_isStarted(HOST_IO());
}

Bool_t test_isRunning()
{
  return HOST_IO_isRunning(HOST_IO());
}

#define ARGN_FXN_BASE(buf,format) \
do{ \
  va_list args; \
  va_start(args,format); \
  vsnprintf(buf,sizeof(buf),format,args); \
} while(0)
static char argnBuf[1024];
void test_log_message(const char * format, ...)
{
  ARGN_FXN_BASE(argnBuf,format);
  HOST_IO_send_log_message(HOST_IO(),argnBuf);
}
Bool_t test_set_control_value(TEST_DataId_t id, const char * format, ...)
{
  ARGN_FXN_BASE(argnBuf,format);
  return HOST_IO_send_element_value(HOST_IO(),TARGET_ControlElement,id,argnBuf);
}
Bool_t test_set_control_params(TEST_DataId_t id, const char * format, ...)
{
  ARGN_FXN_BASE(argnBuf,format);
  return HOST_IO_send_element_params(HOST_IO(),TARGET_ControlElement,id,argnBuf);
}
Bool_t test_set_status_value(TEST_DataId_t id, const char * format, ...)
{
  ARGN_FXN_BASE(argnBuf,format);
  return HOST_IO_send_element_value(HOST_IO(),TARGET_StatusElement,id,argnBuf);
}

Bool_t test_set_plot_params(TEST_DataId_t id, const char * format, ...)
{
  ARGN_FXN_BASE(argnBuf,format);
  return HOST_IO_send_plot_command_ext(HOST_IO(),id,TARGET_plotCmd_setParam, argnBuf);
}

Bool_t test_plot_clear(TEST_DataId_t plotId)
{
  return HOST_IO_send_plot_command(HOST_IO(),plotId,TARGET_plotCmd_Clear);
}

Bool_t test_set_samples(TEST_DataId_t id, Sample_t * Buf, Size_t Count)
{
  return HOST_IO_send_samples(HOST_IO(),id,Buf,Count);
}
Bool_t test_set_samples_iq(TEST_DataId_t id, iqSample_t * Buf, Size_t Count)
{
  return HOST_IO_send_samples_iq(HOST_IO(),id,Buf,Count);
}
